import os

import tarfile
import logging
import filemapper as fm

from datetime import datetime


logging.basicConfig(level=logging.DEBUG)


def readlog(filepath):
    users = {}
    try:
        tar = tarfile.open(filepath, 'r')
        tar.extractall('./temp')
        all_files = fm.load('temp')

        for event in all_files:
            f = open(os.path.join('./temp', event))
            while True:
                l = f.readline()
                if not l:
                    break
                parts = l.split('|')
                if len(parts) < 2:
                    continue
                if parts[1] in users:
                    users[parts[1]].append(parts[0])
                else:
                    users[parts[1]] = [parts[0]]

    except tarfile.ReadError as error:
        logging.error(error)
        logging.debug("Wrong file format")

    users_ids = users.keys()
    user_visiting = {}
    for i in users_ids:
        user_timestamps = users.get(i)
        prev_ts = None
        for ts in user_timestamps:
            if prev_ts is None:
                prev_ts = ts
                continue
            formatted_date = datetime.strptime(ts, '%Y-%m-%dT%H:%M:%S') - datetime.strptime(prev_ts, '%Y-%m-%dT%H:%M:%S')
            prev_ts = ts
            if formatted_date.total_seconds() <= 3600:
                if i in user_visiting:
                    user_visiting[i].append(formatted_date.total_seconds())
                else:
                    user_visiting[i] = [formatted_date.total_seconds()]

    user_visiting_ids = user_visiting.keys()
    spiders_id = []
    for user in user_visiting_ids:
        arr = user_visiting.get(user)
        count = sum(arr)/len(arr)
        if count < 5:
            spiders_id.append(user)

    with open('ids.txt', 'w') as fp:
        fp.write("".join(spiders_id))

    print(len(spiders_id))
    return spiders_id


readlog('events.tar.gz')
